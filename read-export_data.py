import pydicom
import urllib.request
import os
from pathlib import Path
import tarfile
import pandas as pd
import sys

pd.options.display.max_rows = 25
pd.options.display.max_columns = 10
pd.options.display.max_colwidth = 100


# change work folder
os.chdir(r'C:\Users\User\Downloads\viz')

# initialize DF to hold data
info_df = pd.DataFrame(columns=['Patient Name', 'Age', 'Sex', 'Institute', 'Study_ID', 'Series_ID',
                                'Creation Time']) #  'CT Duration'

# define log function
def log_results(*args, **kwargs):
    print(*args, **kwargs)
    with open(r'log_file.txt', 'a') as logfile:
        print(file=logfile, *args, **kwargs)

# retrieve file from URL
filename = 'DICOM_data.tgz'
# url = 'https://s3.amazonaws.com/viz_data/DM_TH.tgz'
url = sys.argv
urllib.request.urlretrieve(url, '{}'.format(filename))


# open and read file, create the necessary hierarchy
tf = tarfile.open(filename, "r:gz")
# iterate on tgz file
for name in tf.getnames():
    f = tf.extractfile(name)
    file_data = pydicom.dcmread(f)


    # extract patient/study/case values
    patient_name = file_data['PatientName'].value
    study_id = file_data['StudyInstanceUID'].value
    series_id = file_data['SeriesInstanceUID'].value

    # create folders based on given values and extract the relevant files into folder
    Path(r'Raw Data\Patient_{}\Study_{}\Series_{}'.format(patient_name, study_id, series_id))\
        .mkdir(parents=True, exist_ok=True)
    tf.extract(name, r'Raw Data\Patient_{}\Study_{}\Series_{}'.format(patient_name, study_id, series_id))

    # generate a data frame to hold information
    patient_age = file_data['PatientAge'].value
    patient_sex = file_data['PatientSex'].value
    institute = file_data['InstitutionName'].value
    ins_creation_time = file_data['ContentTime'].value

    info_df = info_df.append({'Patient Name': patient_name, 'Age': patient_age, 'Sex': patient_sex,
                              'Institute': institute, 'Study_ID': study_id, 'Series_ID': series_id,
                              'Creation Time': ins_creation_time}, ignore_index=True)

# generate raw info file
info_df['Hours'] = info_df['Creation Time'].str.slice(stop=2).astype(int)
info_df['Minutes'] = info_df['Creation Time'].str.slice(start=2, stop=4).astype(int)
info_df['Seconds'] = info_df['Creation Time'].str.slice(start=4, stop=6).astype(int)
info_df['Time'] = info_df['Hours']*3600 + info_df['Minutes']*60 + info_df['Seconds'].astype(int)
info_df.to_csv('info.csv', encoding='utf-8', index=False)

# get average CT times
time_df = info_df.sort_values(by='Time').groupby(['Study_ID', 'Series_ID'])['Time'].\
    apply(lambda x: x.max() - x.min()).reset_index(name='Duration')

# generate results file
inst = info_df['Institute'].unique()
log_results("\nData comes from {} number of hospitals, \nwhich are {}\n".format(len(inst), inst))

patient_df = info_df[['Patient Name', 'Age', 'Sex']].drop_duplicates(subset=['Patient Name', 'Age', 'Sex'])
log_results("List of patient information:\n {} seconds".format(patient_df.to_string(index=False)))

average_ct = time_df['Duration'].mean()
log_results("\nThe average duration of a CT scan is: {}".format(average_ct))